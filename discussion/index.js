		// This is a single line comment
		/* 
			This is a
			multi line
		 	comment 
			shortcut key: ctrl + shft + / (windows)

		*/

		// alert("Hello World");
		// console.log("Hi");
		// console.log(3+2);
		// Variables

		let name;
		name = 'Zuitt';
		name = "Emmanuel"

		let age = 22;

		console.log(name)

		const boilingPoint = 100
		// boilingPoint = 200;

		console.log(boilingPoint);
		console.log(age + '3');
		console.log(name + 3)

		// Boolean
		let isAlive = true;
		console.log(isAlive)

		// Arrays
		let grades = [98, 96, 95, 90];

		let batch197 = ["marnel", "jesus"]

		let grade1 = 98;
		let grade2 = 96;
		let grade3 = 95;
		let grade4 = 90;

		grades[10] = 200

		console.log(grades[10]);
		let movie = "jaws"
		// console.log(song);
		let isp;
		console.log(isp);

		// null vs undefined
		// Undefined - represents the state of a variable that has been declared but without an assigned value

		// null - used intentionally to express the absence of a value in a declared/ initialized variable

		let spouse = null
		console.log(spouse)

		// Objects - special kind of data type that is used to mimic real world objects/items
		/*
			Syntax:
				let objectName = { 
					property1: keyValue1,
					property2: keyValue2,
					property3: keyValue3,
				}
		*/

		let myGrades = {
			firstGrading: 98,
			secondGrading: 92,
			thirdGrading: 90,
			fourthGrading: 94.6
		}

		console.log(myGrades)

		let person = {
			fullName: 'Alonzo Cruz',
			age: 25,
			isMarried: false,
			contact: ['091945687565', '09123456678'],
			address:{
				houseNumber: 345,
				city: 'Manila'
			} 
		}

		console.log(person)
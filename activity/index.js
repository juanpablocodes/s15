console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName;
	firstName = 'First Name: Paul'
	console.log(firstName)

	let lastName;
	lastName = 'Last Name: Galela'
	console.log(lastName)

	let age;
	age = 'age:  ' + 26;
	console.log(age)

	let myHobbies = ["Listening to music", "Watching Movies", "Coding"]
	console.log("Hobbies:"),
	console.log(myHobbies)

	let workAddress = {
		houseNumber: 1,
		street: "San Ponciano",
		city: "Quezon City",
		state: "NCR"
	}
	console.log("Work Address:")
	console.log(workAddress)

	let fullName = "Steve Rogers";
	console.log("My full name is:  " + "Steve Rogers");

	let currentAge = 40;
	console.log("My current age is: " + 40);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are:")
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
	}
	console.log("My Full Profile:")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	lastLocation: "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



	// let myFullName;
	// myFullName = "My full name is: Steve Rogers"
	// console.log(myFullName)

	// let myCurrentAge;
	// myCurrentAge = 'My current age is:  ' + 40;
	// console.log(myCurrentAge)

	// let myFriends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"]
	// console.log("My Friends are:")
	// console.log(myFriends)

	// let fullProfile = {
	// 	userName: "captain_america",
	// 	fullName: "Steve Rogers",
	// 	age: 40,
	// 	isActive: false
	// }
	// console.log("My Full Profile:")
	// console.log(fullProfile)

	// let bestFriend;
	// bestFriend = "My bestfriend is: Bucky Barnes"
	// console.log(bestFriend)

	// let found;
	// found = "I was found in: Arctic Ocean"
	// console.log(found)